package com.magarex.memelord.utils;

/**
 * Created by sahil on 14/11/18.
 **/
public class FirebasePaths {

    public static final String PHOTOS_DB_PATH = "photos";
    public static final String POSTS_DB_PATH = "posts";
    public static final String POSTS_STORAGE_PATH = "posts";
    public static final String PHOTOS_TIMESTAMP_PATH = "timestamp";
    public static final String PHOTOS_UPVOTE_COUNT_PATH = "upvoteCount";

    public static final String STORAGE_PHOTOS_PATH = "photos";
    public static final String STORAGE_PROFILE_PIC_PATH = "profiles";

    public static final String USERS_DB_PATH = "users";
    public static final String USER_ID_PATH = "userId";
    public static final String USER_PIC_PATH = "profilePicURL";
    public static final String USER_NAME_PATH = "displayName";
    public static final String USER_PROVIDER_PATH = "signInService";
    public static final String USER_PROVIDER_IDENTIFIER_PATH = "signInServiceIdentifier";
    public static final String USER_FCM_TOKEN_PATH = "fcmToken";
    public static final String USER_NOTIFICATION_PATH = "notification";

}
