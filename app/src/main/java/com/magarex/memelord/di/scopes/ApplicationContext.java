package com.magarex.memelord.di.scopes;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
